import sqlite3

import settings


class Table(object):
    table_name = None
    columns = None

    def __init__(self, exclude_serialize=None, **kwargs):
        if exclude_serialize is None:
            self.exclude_serialize = set()
        else:
            self.exclude_serialize = exclude_serialize
        self.update(**kwargs)

    @classmethod
    def get_columns_str(cls, with_table_name=False):
        return ', '.join(map(
            lambda x: '"{}"'.format(x),
            cls.columns
        ))

    def get_column_value(self, column):
        try:
            return getattr(self, '%s_dump' % column)()
        except AttributeError:
            return getattr(self, column)

    @property
    def data(self):
        return {
            key: self.get_column_value(key) for key in self.columns
            if key not in self.exclude_serialize
        }

    def update(self, **kwargs):
        for key, value in kwargs.items():
            try:
                value = getattr(self, '%s_load' % key)(value)
            except AttributeError:
                pass
            setattr(self, key, value)


class KladrObjMixin(object):
    columns = [
        'id', 'name', 'name_lower', 'typeShort', 'zip', 'actual'
    ]


class Kladr(KladrObjMixin, Table):
    table_name = 'kladr'


class Street(KladrObjMixin, Table):
    table_name = 'street'


class Building(KladrObjMixin, Table):
    table_name = 'building'


class DB(object):
    _pool = None

    def __init__(self, pool=None):
        self._connection = sqlite3.connect(settings.DBNAME)
        self._cursor = self._connection.cursor()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self._connection.close()

    def set_autocommit(self):
        self._connection.isolation_level = None

    def commit(self):
        self._connection.commit()

    def rollback(self):
        self._connection.rollback()

    def execute(self, query):
        self._cursor.execute(query)

    def _create_entity(self, model, data):
        query = """
            INSERT INTO {table_name}
                ({columns})
                VALUES ({values})
        """.format(
            table_name=model.table_name,
            columns=model.get_columns_str(),
            values=', '.join(map(lambda x: '?', model.columns))
        )
        self._cursor.execute(
            query, tuple(map(lambda x: data[x], model.columns))
        )

    def create_kladr(self, data):
        return self._create_entity(Kladr, data)

    def create_street(self, data):
        return self._create_entity(Street, data)

    def create_building(self, data):
        return self._create_entity(Building, data)

    def update_entity(self, instance):
        query = """
            UPDATE {table_name}
            SET {set}
            WHERE {table_name}.id = {pk}
        """.format(
            table_name=instance.table_name,
            set=', '.join(map(
                lambda x: '"{}" = ?'.format(x),
                instance.columns
            )),
            pk=instance.id
        )
        self._cursor.execute(
            query,
            tuple(map(lambda x: getattr(instance, x), instance.columns))
        )

    def _get_entity(self, model, pk):
        record = next(
            self._cursor.execute('SELECT {} FROM {} WHERE id = {};'.format(
                model.get_columns_str(), model.table_name, pk
            )),
            None
        )
        if record is not None:
            record = {
                model.columns[ind]: x
                for ind, x in enumerate(record)
            }
            return model(**record)
        return None

    def get_kladr_list(self, model, id_start=None, name=None, limit=100):
        query = """
            SELECT
                {table_name}.id
                ,{table_name}.name
                ,{table_name}.name_lower
                ,{table_name}.typeShort
                ,{table_name}.zip
                ,{table_name}.actual
            FROM
                {table_name}
            WHERE
                {where}
            LIMIT {limit};
        """
        where = '1=1'
        if id_start is not None:
            where += '\n AND {table_name}.id >= "{id_start}" '\
                'AND {table_name}.id < "{id_start}A"'.format(
                    table_name=model.table_name,
                    id_start=id_start
                )
        if name is not None:
            where += '\n AND {table_name}.name_lower LIKE "%{name}%"'.format(
                table_name=model.table_name,
                name=name
            )
        res = list(self._cursor.execute(query.format(
            table_name=model.table_name,
            where=where,
            limit=limit,
        )))
        for row in res:
            record = {
                model.columns[ind]: x
                for ind, x in enumerate(row)
            }
            record['typeShort'] = record['typeShort'].lower()
            yield model(exclude_serialize={'name_lower'}, **record)
