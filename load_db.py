import argparse
import datetime
import os

from dbfread import DBF

import settings
from db import DB

parser = argparse.ArgumentParser(description='Load kladr')
parser.add_argument('--dest', help='dbf destination', required=True)

args = parser.parse_args()

t1 = datetime.datetime.now()


def create_tables(db):
    query = """
        CREATE TABLE IF NOT EXISTS
            {table_name}
            (
                id varchar(255) PRIMARY KEY
                , name varchar(255) NOT NULL
                , name_lower varchar(255) NOT NULL
                , typeShort varchar(50) NOT NULL
                , zip varchar(6) NOT NULL
                , actual varchar(2) NOT NULL
            );
    """
    db.execute(query.format(table_name='kladr'))
    db.execute('CREATE INDEX kladr_name_lower ON kladr (name_lower);')
    db.execute(query.format(table_name='street'))
    db.execute(query.format(table_name='building'))
    db.commit()


def prepare_db():
    try:
        os.remove(settings.DBNAME)
    except OSError:
        pass

    db = DB()
    create_tables(db)
    return db

db = prepare_db()


for record in DBF(os.path.join(args.dest, 'KLADR.DBF'), encoding='cp866'):
    data = {
        'id': record['CODE'],
        'name': record['NAME'],
        'name_lower': record['NAME'].lower(),
        'typeShort': record['SOCR'],
        'zip': record['INDEX'],
        'actual': record['CODE'][-2:],
    }
    db.create_kladr(data)
db.commit()


# for record in DBF(os.path.join(args.dest, 'ALTNAMES.DBF'), encoding='cp866'):
#     print(record)


for record in DBF(os.path.join(args.dest, 'DOMA.DBF'), encoding='cp866'):
    data = {
        'id': record['CODE'],
        'name': record['NAME'],
        'name_lower': record['NAME'].lower(),
        'typeShort': record['SOCR'],
        'zip': record['INDEX'],
        'actual': record['CODE'][-2:],
    }
    db.create_building(data)
db.commit()


# for record in DBF(os.path.join(args.dest, 'FLAT.DBF'), encoding='cp866'):
#     print(record)
#     break


# for record in DBF(os.path.join(args.dest, 'SOCRBASE.DBF'), encoding='cp866'):
#     print(record)


for record in DBF(os.path.join(args.dest, 'STREET.DBF'), encoding='cp866'):
    data = {
        'id': record['CODE'],
        'name': record['NAME'],
        'name_lower': record['NAME'].lower(),
        'typeShort': record['SOCR'],
        'zip': record['INDEX'],
        'actual': record['CODE'][-2:],
    }
    db.create_street(data)
db.commit()


# for key, item in region.items():
#     print(key, item)


# for key, item in district.items():
#     print(key, item)


# for key, item in city.items():
#     print(key, item)


print('Закончил разбор', datetime.datetime.now() - t1)
