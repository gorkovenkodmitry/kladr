from cgi import parse_qs
import re
import traceback

import ujson as json

from db import DB
import status
import views


urls = {
    'GET': (
        (
            re.compile(r'^/?$'),
            views.kladr,
        ),
    ),
}


class Request(object):
    query_string = None
    body = None

    def __init__(self, env):
        self.path = env['PATH_INFO']
        self.method = env['REQUEST_METHOD']
        if self.method == 'GET':
            self.query_string = parse_qs(env['QUERY_STRING'], True)
        elif self.method == 'POST':
            self.body = env['wsgi.input'].read()

    def get_json(self):
        return json.loads(self.body.decode())


class Response(object):
    def __init__(self, content, status='200 OK'):
        if isinstance(content, str):
            content = content.encode()
        self._content = (content,)
        self._status = status

    @property
    def content(self):
        return self._content

    @property
    def status(self):
        return self._status

    @property
    def headers(self):
        return [
            ('Content-Type', 'application/json'),
            ('Connection', 'close'),
            ('Content-Length', str(len(self.content[0]))),
        ]


def response(request, db):
    for url in urls.get(request.method, []):
        m = url[0].search(request.path)
        if m is not None:
            try:
                return Response(*url[1](db, request, **m.groupdict()))
            except Exception:
                print('############################')
                print('PATH:', request.path)
                print('METHOD:', request.method)
                print('QS:', request.query_string)
                print('BODY:', request.body)
                traceback.print_exc()
                print('############################')
                return Response('', status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response('', status.HTTP_404_NOT_FOUND)


def application(env, start_response):
    with DB() as db:
        r = response(Request(env), db)
    start_response(r.status, r.headers)
    for x in r.content:
        yield x
