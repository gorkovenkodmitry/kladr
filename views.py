import copy
import ujson as json

import status
from db import Kladr, Street, Building


def parse_query_string(query_string, params_map, escape=False):
    params = {}
    for key, value in params_map.items():
        if key not in query_string:
            continue
        param = params_map[key](query_string[key][0])
        if escape:
            if isinstance(param, int):
                params[key] = param
            else:
                params[key] = repr(param)
        else:
            params[key] = param
    return params


def validate_limit(search_context):
    search_context = copy.deepcopy(search_context)
    try:
        if search_context['limit'] > 100:
            search_context['limit'] = 100
    except KeyError:
        search_context['limit'] = 100
    return search_context


def kladr_relevant_sort(rows, query):
    if not query:
        return sorted(rows, key=lambda x: x['id'])

    class K:
        def __init__(self, obj, *args):
            self.obj = obj
            self.find_ind = self.obj['name'].lower().find(query)

        def __lt__(self, other):
            if query == self.obj['name'].lower():
                return True
            return self.find_ind < other.find_ind

        def __gt__(self, other):
            if query == other.obj['name'].lower():
                return True
            return self.find_ind > other.find_ind

        def __eq__(self, other):
            return all(
                self.obj['name'].lower() == other.obj['name'].lower(),
                self.find_ind == other.find_ind
            )

        def __le__(self, other):
            return self.find_ind <= other.find_ind

        def __ge__(self, other):
            if query == self.obj['name'].lower():
                return True
            return self.find_ind >= other.find_ind

        def __ne__(self, other):
            return self.find_ind != other.find_ind

    return sorted(rows, key=K)


def search_kladr(db, request):
    params_map = {
        'contentType': str,
        'limit': int,
        'query': str,
    }
    search_context = parse_query_string(request.query_string, params_map)
    search_context = validate_limit(search_context)

    query = search_context.get('query', '').lower()
    result = db.get_kladr_list(
        Kladr,
        name=query,
        limit=search_context['limit']
    )

    return json.dumps({
        'searchContext': search_context,
        'result': kladr_relevant_sort(
            [x.data for x in result],
            query
        )
    }), status.HTTP_200_OK


def search_street(db, request):
    params_map = {
        'contentType': str,
        'limit': int,
        'cityId': str,
        'query': str,
    }
    search_context = parse_query_string(request.query_string, params_map)
    search_context = validate_limit(search_context)

    query = search_context.get('query', '').lower()
    result = db.get_kladr_list(
        Street,
        id_start=search_context.get('cityId', '')[:11],
        name=query,
        limit=search_context['limit']
    )

    return json.dumps({
        'searchContext': search_context,
        'result': kladr_relevant_sort(
            [x.data for x in result],
            query
        )
    }), status.HTTP_200_OK


def search_building(db, request):
    params_map = {
        'contentType': str,
        'limit': int,
        'streetId': str,
        'query': str,
    }
    search_context = parse_query_string(request.query_string, params_map)
    search_context = validate_limit(search_context)

    query = search_context.get('query', '').lower()
    result = db.get_kladr_list(
        Building,
        id_start=search_context.get('streetId', '')[:15],
        name=query,
    )
    data = []
    for x in result:
        row = x.data
        for name in row['name'].split(','):
            if name.lower().find(query) == -1:
                continue
            new_obj = dict(**row)
            new_obj['name'] = name
            data.append(new_obj)

    return json.dumps({
        'searchContext': search_context,
        'result': kladr_relevant_sort(
            data[:search_context['limit']],
            query
        )
    }), status.HTTP_200_OK


SEARCH_FUNC_MAP = {
    'region': search_kladr,
    'district': search_kladr,
    'city': search_kladr,
    'street': search_street,
    'building': search_building,
}


def kladr(db, request):
    try:
        content_type = request.query_string.get('contentType')[0]
    except (AttributeError, IndexError):
        return (
            json.dumps({'detail': 'contentType required parameter'}),
            status.HTTP_400_BAD_REQUEST,
        )
    try:
        return SEARCH_FUNC_MAP[content_type](db, request)
    except KeyError:
        return (
            json.dumps({'detail': 'contentType incorrect'}),
            status.HTTP_400_BAD_REQUEST,
        )
